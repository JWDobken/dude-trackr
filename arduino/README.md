# Dude Trackr Arduino

## Handly links

* http://support.sodaq.com/sodaq-one/arduino-ide-setup/
* https://support.sodaq.com/sodaq-one/sodaq-one/

## Prerequisites

* Install Aduino IDE (vscode plugin uses that)): https://www.arduino.cc/en/Guide/MacOSX
    * Make sure it's in your Applications folder
    * Make sure this app once
    * Add http://downloads.sodaq.net/package_sodaq_samd_index.json in the Additional Boards manager URL
    * Go to Tools > Boards > Boards Manager. Search for SODAQ and install
* Arduino plugin for VSCode: https://medium.com/home-wireless/use-visual-studio-code-for-arduino-2d0cf4c1760b
    * Make sure you restarted VSCode after you've installed the Arduino IDE

## Installation

* Open up VSCode
* Connect the SODAQ One to USB
* Hit install in the popup you get and wait, quite a long time

## Initial Upload

* Select Serial Port (`Bluetooth incoming port`) from the menu in the bottom right corner.
    * This setting can now also be found in `./.vscode/arduino.json`
* `cmd + shift + P` => `Arduino: upload`
    * Starts up the Arduino IDE again